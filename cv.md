# Gerard Bystron 
## Student on App Development
### About

I'm a studyng app development, I'm a polyvalent worker that can programme, administrate servers, repair and mount computers, design creative apps for many platforms, and try to help and teach to others among the way.

---
### Experience
- Years of training in creative teamwork and improvisation.
 
- Helper in a cooperative that helps poor people both in our city and the 3rd world. (Adsis 1 year)
---
### Education And Qualifications

- Advanced Certificate *(2019)*  

- Bachellors Degree Specialized on Tech and Science *(2019)*

- Vocational course in App Development *(2021)*
---
### Skills 
- Work well under pressure. 
- Patient and professional. 
- In constant formation. 
- Video editing with Sony Vegas
---
### Contact
**+34 693 252 920**

gerardbystron@hotmail.com
